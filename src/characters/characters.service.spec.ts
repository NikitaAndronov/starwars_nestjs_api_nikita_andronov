import {Test} from "@nestjs/testing"
import { CharactersRepository } from "./characters.repository";
import { CharactersService } from "./characters.service";

const mockCharactersRepository = () => ({
    getCharacters: jest.fn()

})

describe("StarWars",()=>{
    let charactersService: CharactersService;
    let charactersRepository //: CharactersRepository;
    
    beforeEach( async () =>{
        // initialize a NestJs module with charactersService
        // and tasksRepository
        const module = await Test.createTestingModule({
            providers :  [
                CharactersService,
                { provide: CharactersRepository, useFactory : mockCharactersRepository},
            ]
        }).compile();
     charactersService = module.get<CharactersService>(CharactersService)
     charactersRepository = module.get<CharactersRepository>(CharactersRepository)
    });

    describe("getCharacters" , () => {
        it('calls CharactersRepository.getCharacters and returns the result', async () =>{
            expect(charactersRepository.getCharacters).not.toHaveBeenCalled()
            //call charactersService.getCharacters, which should then call the repository's getCharacters
            charactersRepository.getCharacters.mockResolvedValue("someValue")
            const result =  await charactersRepository.getCharacters();
            expect(charactersRepository.getCharacters).toHaveBeenCalled();
            expect(result).toEqual("someValue")
        });
    
    });



});