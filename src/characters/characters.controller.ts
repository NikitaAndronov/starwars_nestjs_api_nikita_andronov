import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { ApiBody } from '@nestjs/swagger';
import { CharactersService } from './characters.service';
import { CreateCharacterDto } from './dto/create-character.dto';
import { UpdateCharacterDto } from './dto/update-character.dto';

@Controller('characters')
export class CharactersController {


    constructor(private charactersService: CharactersService) {
        this.charactersService = charactersService;

    }

    @Get()
    getAllCharacters(@Req() req): any {
        console.log(req.query);
        return this.charactersService.getCharacters(parseInt(req.query.take), parseInt(req.query.skip));
    }

    @Post()
    @ApiBody( {type: CreateCharacterDto })
    async createCharacters(@Body() createCharacterDto: CreateCharacterDto) {
        return await this.charactersService.createChapter(createCharacterDto);
    }


    @Put()
    @ApiBody( {type: UpdateCharacterDto })
    async updateCharacters(@Body() updateCharacterDto: UpdateCharacterDto) {
        return await this.charactersService.updateChapter(updateCharacterDto);
    }

    @Delete("/all")
    //: Characters
    async deleteAllCharacters() {
        return await this.charactersService.deleteAllCharacters();
    }

    @Delete("/:id")
    //: Characters
    async deleteCharacter(@Param('id') id: string,) {
        return await this.charactersService.deleteCharacter(id);
    }


}
