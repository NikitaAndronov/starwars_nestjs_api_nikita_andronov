import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsNotEmpty, IsString } from "class-validator";

export class CreateCharacterDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    chapter : string;

    @ApiProperty()
    @IsNotEmpty()
    @IsArray()
    episodes  : string[];

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    planet : string;
}

