import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { CreateCharacterDto } from "./create-character.dto";

export class UpdateCharacterDto extends CreateCharacterDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    id : string;

}