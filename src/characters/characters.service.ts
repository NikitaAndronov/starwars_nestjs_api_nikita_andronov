import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectID } from 'typeorm';
import { ICharacter, IGetCharactersResponse } from './characters.model';
import { CharactersRepository } from './characters.repository';
import { CreateCharacterDto } from './dto/create-character.dto';
import { UpdateCharacterDto } from './dto/update-character.dto';

@Injectable()
export class CharactersService {
    

    constructor(
        @InjectRepository(CharactersRepository)
        private characterRepository : CharactersRepository
    ){}


         async  deleteAllCharacters() {
            return this.characterRepository.deleteAllCharacters();
        }

        deleteCharacter(id : string) {
            return this.characterRepository.deleteCharacter(id);
        }
        



        async createChapter(createCharacterDto : CreateCharacterDto) : Promise<ObjectID>{
            const {chapter, episodes, planet} = createCharacterDto
            return this.characterRepository.createChapter(createCharacterDto)
        
        }

        async updateChapter(updateCharacterDto : UpdateCharacterDto) : Promise<ObjectID>{
            return this.characterRepository.updateCharacters(updateCharacterDto)
        
        }

        async   getCharacters(take,skip) {
            const [result, total] = await this.characterRepository.findAndCount({skip: skip ,take : take});
            const response : IGetCharactersResponse  = {} as IGetCharactersResponse;
            let oneEntity  : ICharacter = {} as ICharacter;
            response.characters = [] as ICharacter[];

            response.characters;
            console.log(result);
            for(const item of result){
                oneEntity.id = item._id;
                oneEntity.name = item.character;
                const episodesName = [];
                item?.episodes?.forEach(episode => episodesName.push(episode));
                oneEntity.episodes = episodesName;
                oneEntity.planet = item?.planet
                response.characters.push(oneEntity); 
                oneEntity =  {} as ICharacter;
            }
        return { total, response}
        }


}
