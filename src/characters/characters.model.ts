import { ObjectID } from "typeorm";

export interface IGetCharactersResponse{
    totalRows : number;
    characters :  ICharacter[]
}

export interface ICharacter { 
    id : ObjectID;
    name : string;
    episodes : string[];
    planet : string;
}