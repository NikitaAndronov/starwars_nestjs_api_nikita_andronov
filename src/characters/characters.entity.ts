import { BaseEntity,  Column, Entity, ObjectID, ObjectIdColumn } from "typeorm";

@Entity()
export class Characters extends BaseEntity {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    character: string;

    @Column()
    planet: string

    @Column()
    episodes: string[]


}