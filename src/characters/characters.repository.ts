import { ConflictException } from "@nestjs/common";
import { EntityRepository, getMongoManager, ObjectID, Repository } from "typeorm";
import { Characters } from "./characters.entity";
import { CreateCharacterDto } from "./dto/create-character.dto";
import { UpdateCharacterDto } from "./dto/update-character.dto";

@EntityRepository(Characters)
export class CharactersRepository extends Repository<Characters>{
    public async deleteCharacter(id : string) {
       const manager = getMongoManager();
       await  this.delete(id);
        // const item = await manager.deleteOne(Characters,{where:{id:id}});
        // console.log(item);
    }
   
    public async createChapter(createCharacterDto: CreateCharacterDto) {
        const { chapter, episodes, planet } = createCharacterDto;
        const characters = new Characters();
        characters.character = chapter;
        characters.episodes = [];
        for (const item of episodes) {
            characters.episodes.push(item);
        }
        characters.planet = planet
        const manager = getMongoManager();
        
        await manager.save(characters);
        return characters._id;

    }

    public async getCharacters(){
       console.log("info: ");
       console.log(await this.createQueryBuilder("characters").getMany());
    }

    public async updateCharacters(updateCharacterDto: UpdateCharacterDto){
        const { id, chapter, episodes, planet } = updateCharacterDto;
        const manager = getMongoManager();
    
        const characters = await this.findOneOrFail(id);
        console.log(characters);
        //const result = await manager.deleteMany(Characters,id);
      
        if(!characters){
            throw new ConflictException(`characters with id = ${id} doesn't exist`);
        }
   
        characters.character = chapter;
        characters.episodes = [];
        for (const item of episodes) {
           characters.episodes.push(item);
        }
       // console.log(characters);
       await characters.save();
   
        return characters._id;
     }
 


     async deleteAllCharacters() {
        const manager = getMongoManager();
        await manager.deleteMany(Characters,{});
       
    }


}