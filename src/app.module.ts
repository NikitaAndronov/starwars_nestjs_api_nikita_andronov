import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CharactersModule } from './characters/characters.module';

@Module({
  imports: [
    CharactersModule,
    TypeOrmModule.forRoot({
      type: "mongodb",
      host: "localhost",
      port: 27017,
      database: "test",
      autoLoadEntities: true,
      synchronize: true,
      useUnifiedTopology: true
    })],
})
export class AppModule {}
